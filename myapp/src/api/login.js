import server from './http.js' //导入axios
// 登录
const login = async (msg) => {
    let data = await server.post('/login', msg)
    return data
}
const userlist = async (msg) => {
    let data = await server.get('/users?' + msg)
    return data
}
const leftbat = async () => {
    let data = await server.get('/menus')
    return data
}
// 用户管理
const adduser = async (msg) => {
    let data = await server.get(`users?query=${msg.query}&pagenum=${msg.pagenum}&pagesize=${msg.pagesize}`)
    return data
}
const addusers = async (msg) => {
    console.log(msg);
    let data = await server.post('users', { username: msg.name, password: msg.pass, email: msg.emil, mobile: msg.phon })
    return data
}
const dele = async (msg) => {
    let data = await server.delete('users/' + msg)
    return data
}
const status = async (msg) => {
    let data = await server.put('users/' + msg.id + "/state/" + msg.type)
    return data
}
const edit = async (msg, params) => {
    console.log(msg, params);
    let data = await server({ url: 'users/' + msg, data: params, method: 'put' })
    return data
}
const admin = async (id, rid) => {
    let data = await server({ url: `users/${id}/role`, data: rid, method: 'put' })
    return data
}
// 权限管理
const role = async () => {
    let data = await server.get('roles')
    return data
}
const addRole = async (name, describe) => {
    let data = await server.post('roles', { roleName: name, roleDesc: describe })
    return data
}
const roless = async () => {
    let data = await server.get('rights/list')
    return data
}
const roledele = async (row, rightid) => {
    let data = await server.delete(`roles/${row.id}/rights/${rightid}`)
    return data
}
const deleted = async (id) => {
    let data = await server.delete('roles/' + id)
    return data
}
const distribution = async () => {
    let data = await server.get('rights/tree')
    return data
}
const change = async (id, str) => {
    let data = await server.post(`roles/${id}/rights`, { rids: str })
    return data
}
const editRole = async (id, name) => {
    let data = await server.put('roles/' + id, { roleName: name.roleName, roleDesc: name.roleDesc })
    return data
}
// 商品列表
const goods = async (msg) => {
    let data = await server.get(`goods?query=${msg.query}&pagenum=${msg.pagenum}&pagesize=${msg.pagesize}`)
    return data
}
const delGoods = async (id) => {
    let data = await server.delete('goods/' + id)
    return data
}
const getList = async () => {
    let data = await server.get('categories')
    return data
}
const clickTab = async (id) => {
    let data = await server.get(`categories/${id}/attributes?sel=many`)
    return data
}
const onlyTab = async (id) => {
    let data = await server.get(`categories/${id}/attributes?sel=only`)
    return data
}
const addList = async (msg) => {
    console.log(msg);
    let data = await server.post('/goods',{goods_name:msg.goods_name,goods_price:msg.goods_price,goods_number:msg.goods_number,goods_weight:msg.goods_weight,goods_cat:msg.goods_cat})
    return data
}
// 商品分类
const cateTab = async (page) => {
    let data = await server.get(`categories?type=${page.type}&pagenum=${page.pagenum}&pagesize=${page.pagesize}`)
    return data
}
const cateAdd = async () => {
    let data = await server.get(`categories?type=2`)
    return data
}
const cateAddList = async (cat) => {
    console.log(cat);
    let data = await server.post('categories', cat)
    return data
}
const cateDeleted = async (id) => {
    let data = await server.delete('categories/' + id)
    return data
}
const cateEdit = async (id, msg) => {
    let data = await server.put('categories/' + id, { cat_name: msg })
    return data
}
// 分类参数
const cateParams = async (id, msg) => {
    let data = await server.get(`categories/${id}/attributes?sel=${msg}`)
    return data
}
const addParams = async (id, msg) => {
    let data = await server.post(`categories/${id}/attributes`, { attr_sel: msg.attr_sel, attr_name: msg.attr_name })
    return data
}
const editParams = async (id, msg) => {
    console.log(msg.name);
    let data = await server.put(`categories/${id.editNameId}/attributes/${id.editId}`, { attr_sel: msg.sel, attr_name: msg.name })
    return data
}
const cateDele = async (id) => {
    let data = await server.delete(`categories/${id.editNameId}/attributes/${id.editId}`)
    return data
}
const cateTag = async (id, msg) => {
    let data = await server.put(`categories/${id.editNameId}/attributes/${id.editId}`, { attr_sel: msg.sel, attr_name: msg.name, attr_vals: msg.vals })
    return data
}
const cateTags = async (id, msg) => {
    let data = await server.put(`categories/${id.editNameId}/attributes/${id.editId}`, { attr_sel: msg.sel, attr_name: msg.name, attr_vals: msg.vals })
    return data
}
// 订单列表
const orderList = async (msg) => {
    let data = await server.get(`orders?query=${msg.query}&pagenum=${msg.pagenum}&pagesize=${msg.pagesize}`)
    return data
}
const reports = async () => {
    let data = await server.get(`reports/type/1`)
    return data
}
export {
    login, userlist, leftbat, adduser, addusers, dele, status, edit,addList, admin, role, roless, goods, roledele, distribution, change, deleted, addRole, editRole, delGoods, getList, clickTab, cateTab, cateAdd, cateAddList, cateDeleted, cateEdit, cateParams, addParams, editParams, cateDele, onlyTab, cateTag, orderList, cateTags, reports
}