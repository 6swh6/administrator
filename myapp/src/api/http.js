import axios from 'axios'
import { Message, MessageBox } from 'element-ui' //在js文件中，需单独引入提示

const server = axios.create({
    // baseURL: 'http://127.0.0.1:8888/api/private/v1',
    baseURL: process.env.VUE_APP_API,
    timeout: 5000
})
//请求拦截
server.interceptors.request.use(config => {
    // console.log(config);
    //给请求头统一加token，不用每次进后台，每个页面都加了
    if (localStorage.getItem('token')) {
        config.headers.Authorization = localStorage.getItem('token');
    }
    return config
}, error => {
    Promise.reject(error)
})
//响应拦截

let createId = true
server.interceptors.response.use(res => {
    //根据自己后端返回状态码，设置统一的错误提醒
    const msg = res.data.meta.msg
    // const status = res.data.meta.status
    if (msg === "无效token") {
        if (createId) {
            createId = false
            MessageBox.confirm('token已过期,是否重新登录?', '系统提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                location.href = "/login"
            }).catch(() => {
                Message({
                    type: 'info',
                    message: '已取消'
                });
            });
        }
    }
    //   if (status !== 200) {
    //     Message.error({
    //       duration:2000,
    //       message: '状态码错误'
    //     })
    //   }
    return res.data

}, error => {
    Promise.reject(error)
})
export default server