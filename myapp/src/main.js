import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import TreeTable from 'vue-table-with-tree-grid'
import echarts from 'echarts/lib/echarts'

Vue.prototype.$echarts = echarts
Vue.config.productionTip = false

import ELEMENT from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ELEMENT);

Vue.component('tree-table', TreeTable)

Vue.filter('dataFormat', function (originVal) {
  const dt = new Date(originVal)

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')

  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getHours() + '').padStart(2, '0')
  const ss = (dt.getHours() + '').padStart(2, '0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
