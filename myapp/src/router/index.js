import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/login",
    // component: () => import('../views/login.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/home',
    name: 'home',
    redirect: '/index',
    component: () => import('../views/Home.vue'),
    children: [{
      path: '/index',
      name: 'index',
      component: () => import('../components/index.vue'),
    }, {
      path: '/users',
      name: 'users',
      component: () => import('../components/users/users.vue'),
    },
    {
      path: '/rights',
      name: 'rights',
      component: () => import('../components/rights/rights.vue'),
    },
    {
      path: '/orders',
      name: 'orders',
      component: () => import('../components/orders/orders.vue'),
    },
    {
      path: '/reports',
      name: 'reports',
      component: () => import('../components/reports/reports.vue'),
    },
    {
      path: '/goods',
      name: 'goods',
      component: () => import('../components/goods/goods.vue'),
    },
    {
      path: '/goods/Add',
      name: 'Add',
      component: () => import('../components/goods/Add.vue'),
    },
    {
      path: '/categories',
      name: 'categories',
      component: () => import('../components/goods/categories.vue'),
    },
    {
      path: '/params',
      name: 'params',
      component: () => import('../components/goods/params.vue'),
    },
    {
      path: '/roles',
      name: 'roles',
      component: () => import('../components/rights/roles.vue'),
    },
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {

  // console.log(to);
  if (to.path != "/login") {
    if (localStorage.getItem("token")) {
      next()
    } else {
      next("/login")
    }
  } else {
    next()
  }
})

export default router
